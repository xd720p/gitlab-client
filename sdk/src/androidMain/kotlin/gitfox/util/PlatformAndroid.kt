package gitfox.util

internal actual fun currentTimeMillis(): Long = System.currentTimeMillis()