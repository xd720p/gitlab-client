//
//  ContentView.swift
//  GitFox iOS Sample
//
//  Created by Konstantin Tskhovrebov on 06.04.2020.
//  Copyright © 2020 Konstantin Tskhovrebov. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
